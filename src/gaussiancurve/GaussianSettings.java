package gaussiancurve;

public class GaussianSettings
{
    private double sigma;
    private double mu;
    private String seriesName;
    
    public GaussianSettings(){}
    
    public void setSigma(double sigma){this.sigma = sigma;}
    public double getSigma(){return sigma;}
    public void setMu(double mu){this.mu = mu;}
    public double getMu(){return this.mu;}
    public void setSeries(String seriesName){this.seriesName = seriesName;}
    public String getSeries(){return seriesName;}
}