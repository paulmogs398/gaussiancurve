package gaussiancurve;

import java.util.ArrayList;

public class GaussianCurve
{
    public static void main(String[] args)
    {
        RenderSettings renderSettings = new RenderSettings();               // Image and run specific settings
        ArrayList<GaussianSettings> gaussianSettings = new ArrayList<>();   // Each series settings

        if(args.length == 9)
        {
            System.out.println("Parsing arguments...");
            GaussianSettings parsedSetting = new GaussianSettings();
            try
            {
                parsedSetting.setSigma(Double.parseDouble(args[0]));
                parsedSetting.setMu(Double.parseDouble(args[1]));
                parsedSetting.setSeries(""); // One nameless series in parameter mode
		renderSettings.setDrawKey(false); // No need for a key with only one series
                renderSettings.setStartAt(Double.parseDouble(args[2]));
                renderSettings.setInterval(Double.parseDouble(args[3]));
                renderSettings.setEndBefore(Double.parseDouble(args[4]));
                renderSettings.setDrawCrosses(Boolean.parseBoolean(args[5]));
                renderSettings.setPrintValues(Boolean.parseBoolean(args[6]));
                renderSettings.setImageXSize(Integer.parseInt(args[7]));
                renderSettings.setImageYSize(Integer.parseInt(args[8]));
            }
            catch (Exception e)
            {
                System.out.println("Error parsing parameters. " + e.getMessage());
                System.exit(0);
            }
            gaussianSettings.add(parsedSetting);
        }
        else if(args.length == 0)
        {
            System.out.println("### Gaussian Curve Interactive Mode ###");
            Cli cli = new Cli();
            gaussianSettings = cli.GetSeriesSetup();
            renderSettings = cli.GetRenderSetup();
        }
        else
        {
            System.out.println("Gaussian Curve, a command line utility for plotting Gaussian Curves.");
            System.out.println("java GaussianCurve [Sigma] [Mu] [StartPlot] [Interval] [EndBefore] [DrawCrosses] [ProntValues] [ImageXSize] [ImageYSize]");
            System.out.println("Example: GaussianCurve 20.5 50 0 3 100 true true 500 500");
            System.out.println("Or use no parameters to access the interactive mode.");
            System.exit(0);
        }

        // automatic or manual naming
        String fileName;
        if((renderSettings.getFileName() == null || renderSettings.getFileName().isEmpty()) && gaussianSettings.size()==1) // There's just one series, if they havn't provided a name then automatically generate a useful one
        {fileName = "Gaussian" + gaussianSettings.get(0).getSigma() + "_" + gaussianSettings.get(0).getMu() + "_" + renderSettings.getImageXSize() + "_" + renderSettings.getImageYSize() + "_" + renderSettings.getStartAt() + "_" + renderSettings.getInterval() + "_" + renderSettings.getEndBefore() + "_" + (renderSettings.getDrawCrosses()?"1":"0");}
        else if (renderSettings.getFileName() == null || renderSettings.getFileName().isEmpty()) // There's more than one series and no applied name
        {fileName = "UntitledGaussianCurve";}
        else{fileName = renderSettings.getFileName();}

	//Check start at is bigger than endbefore
	if(renderSettings.getStartAt() > renderSettings.getEndBefore() || renderSettings.getInterval() <= 0)
	{
	   System.out.println("Error. Start position must be before end position and interval must be posative.");
	   System.exit(0);
	}

        // Plot each series
        System.out.println("Plotting curve(s)...");
        ArrayList<DataPoint> myCurve = new ArrayList<>();
        Gaussian myGaussian = new Gaussian();
        for(GaussianSettings gs : gaussianSettings)
        {
            myGaussian.setSigma(gs.getSigma());
            myGaussian.setMu(gs.getMu());
            for(double x = renderSettings.getStartAt(); x < renderSettings.getEndBefore(); x = x + renderSettings.getInterval())
            {
                myCurve.add(new DataPoint(x,myGaussian.computeGaussian(x),gs.getSeries()));
            }
        }

        // Print it
        System.out.println("Printing values...");
        if(renderSettings.getPrintValues())
        {
            for(DataPoint dp : myCurve)
            {
                if(dp != null){System.out.println("x" + dp.getX() + " y" + dp.getY());}
                else{System.out.println("NULL");}
            }
        }

        // Render
        System.out.println("Rendering...");
        try{new Render1D(renderSettings.getImageXSize(), renderSettings.getImageYSize(), fileName , myCurve.toArray(new DataPoint[myCurve.size()]), renderSettings.getDrawCrosses(), renderSettings.getDrawKey());}
        catch(Exception e){System.out.println("Error rendering/writing. " + e.getMessage());}

        System.out.println("Done!");
    }
}
