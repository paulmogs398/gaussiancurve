package gaussiancurve;

import static java.lang.Math.exp;
import static java.lang.Math.pow;

public class Gaussian
{
    private double sigma = 1.0;
    private double mu = 1.0;
    private double a = Double.NaN; //Nan to indicate it hasn't yet been computed
    
    public Gaussian(){}
    
    public double getMu(){return mu;}    
    public void setMu(double centre){this.mu = centre;}    
    public double getSigma(){return sigma;}    
    public void setSigma(double sigma)
    {
        this.sigma = sigma;
        a = Double.NaN; // needs to be recomputed
    }
    
    private void setA()
    {
        a = 1 / (sigma * (Math.sqrt((2 * Math.PI))));
    }
    
    public double computeGaussian(double x)
    {
        if(Double.isNaN(a))
        {
            // "a" needs to to be computed
            setA();
        }
        
        return a * (exp(-0.5 * pow(((x - mu) / sigma),2)));
    }
    
}
