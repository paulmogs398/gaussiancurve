package gaussiancurve;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.imageio.ImageIO;


public class Render1D
{
    private BufferedImage image;

    public Render1D(int imageX, int imageY, String fileName , DataPoint[] graph, boolean drawCrosses, boolean drawKey)
            throws Exception
    {
        // Check input
        if(imageX < 30){throw new Exception("Image X size must be posative and atleast 30");}
        if(imageY < 30){throw new Exception("Image Y size must be posative and atleast 30");}
        if(fileName.isEmpty()){throw new Exception("Please provide a file name");}
        if(graph.length < 1){throw new Exception("Please provide some data to draw");}
        int margin = 20;

        // Get ready to draw
        image = new BufferedImage(imageX, imageY, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics(); 
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, imageX, imageY);

        // Find highest and lowest data
        double highestXPoint = 0;
        double lowestXPoint = 0;
        double highestYPoint = 0;
        double lowestYPoint = 0;
        for(DataPoint dp : graph)
        {
            if(highestXPoint < dp.getX()){highestXPoint = dp.getX();}
            if(lowestXPoint > dp.getX()){lowestXPoint = dp.getX();}
            if(highestYPoint < dp.getY()){highestYPoint = dp.getY();}
            if(lowestYPoint > dp.getY()){lowestYPoint = dp.getY();}
        }
        double xSpan = Math.abs(highestXPoint) + Math.abs(lowestXPoint);
        double ySpan = Math.abs(highestYPoint) + Math.abs(lowestYPoint);

        // Plot
        HashMap<String,Color> series = new HashMap<String,Color>();
        HashMap<String,DataPoint> lastPoints = new HashMap<String,DataPoint>();
        for(DataPoint dp : graph)
        {
            // Get the color of the pen
            if(series.containsKey(dp.getSeries()))
            {
                g.setColor(series.get(dp.getSeries()));
            }
            else
            {
                // The first time we have seen this data series assign a plot a colour
                series.put(dp.getSeries(),getRandomBrightColor());
                g.setColor(series.get(dp.getSeries()));
            }

            // Calculate point position
            int cx = Integer.parseInt(Math.round((dp.getX() / xSpan) * (imageX - (margin*2))) + "") + margin;
            int cy = imageY/2 - Integer.parseInt(Math.round((dp.getY() / ySpan) * (imageY/2 - (margin*2))) + "") - margin; // invert y because of graphic cords layout

            // If required draw a cross
            if(drawCrosses)
            {
                g.drawLine(cx-5, cy-5, cx+5, cy+5);
                g.drawLine(cx-5, cy+5, cx+5, cy-5);
            }

            // Get the last point if any
            DataPoint last;
            if(lastPoints.containsKey(dp.getSeries()))
            {
                // This is where we will draw the line from
                last = lastPoints.get(dp.getSeries());

                // Scale to fit image
                int lx = Integer.parseInt(Math.round((last.getX() / xSpan) * (imageX - (margin*2))) + "") + margin;
                int ly = imageY/2 - Integer.parseInt(Math.round((last.getY() / ySpan) * (imageY/2 - (margin*2))) + "") - margin;// invert y because of graphic cords layout

                // Draw the line
                g.drawLine(lx, ly, cx, cy);

                // Set last point to the current point ready for the next iteration
                lastPoints.put(dp.getSeries(), dp);
            }
            else
            {
                // No drawing required just set last point to current
                lastPoints.put(dp.getSeries(), dp);
            }
        }

	//Draw key
	if(drawKey)
	{
		int y = margin;
		for(Map.Entry<String,Color> curve : series.entrySet())
		{
			g.setColor(curve.getValue());
			g.drawLine(margin, y-5, margin + 10, y-5);
			g.drawString(curve.getKey(),margin + 15, y);
			y = y + 20;
		}
	}

        //Write image
        File outputfile = new File(fileName + ".gif");
        ImageIO.write(image, "gif", outputfile);
    }

    private static Color getRandomBrightColor()
    {
        Random rand = new Random();
        return new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()).brighter();
    }
}
