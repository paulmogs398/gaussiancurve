package gaussiancurve;

import java.util.ArrayList;
import java.util.Scanner;

public class Cli
{
    private final Scanner term = new Scanner(System.in);

    public Cli(){}

    public ArrayList<GaussianSettings> GetSeriesSetup()
    {
        ArrayList<GaussianSettings> settings = new ArrayList<>();
        while(true)
        {
            GaussianSettings setting = new GaussianSettings();
            println("Please enter a Sigma: (This influences the width of Gaussian Curve)");
            setting.setSigma(readDouble());
            println("Please enter a Mu: (This positions the Gaussian Curve)");
            setting.setMu(readDouble());
            println("Please enter a name for this series/curve.");
            setting.setSeries(readString());

            boolean nameAlreadyUsed = false;
            for(GaussianSettings gs : settings)
            {
                if(gs.getSeries().equals(setting.getSeries()))
                {
                    nameAlreadyUsed = true;
                    break;
                }
            }

            if(nameAlreadyUsed)
            {
                printError("Name already used! Series not added!");
                println("Please try again");
                continue;
            }
            else
            {
                settings.add(setting);
            }

            println("Would you like to add another series?");
            if(!readBoolean()){break;}
        }
        return settings;
    }

    public RenderSettings GetRenderSetup()
    {
        RenderSettings setting = new RenderSettings();

        println("Render the curves from: ");
        setting.setStartAt(readDouble());
        println("Sampling every: ");
        setting.setInterval(readDouble());
        println("Until: ");
        setting.setEndBefore(readDouble());
        println("Draw \"x\"\'s to mark samples?");
        setting.setDrawCrosses(readBoolean());
        println("Print out the values here?");
        setting.setPrintValues(readBoolean());
	println("Display a key on image?");
	setting.setDrawKey(readBoolean());
        println("Please enter the size of the image you wish to render.");
        print(" x: ");
        setting.setImageXSize(readInt());
        print(" y: ");
        setting.setImageYSize(readInt());
        println("Please enter a name for the file: (w/o extension)");
        setting.setFileName(readString());

        return setting;
    }

    private int readInt()
    {
        while(true)
        {
            String userInput = term.nextLine();
            if(userInput==null){printError("Null input!");}
            else if(userInput.isEmpty()){printError("No input!");}
            else
            {
                try{return Integer.parseInt(userInput);}
                catch(Exception e){printError("Non int input!");}
            }
            println("Please try again!");
        }
    }

    private double readDouble()
    {
        while(true)
        {
            String userInput = term.nextLine();
            if(userInput==null){printError("Null input!");}
            else if(userInput.isEmpty()){printError("No input!");}
            else
            {
                try{return Double.parseDouble(userInput);}
                catch(Exception e){printError("Non double input");}
            }
            println("Please try again!");
        }
    }

    private boolean readBoolean()
    {
        while(true)
        {
            String userInput = term.nextLine();
            if(userInput==null){printError("Null input!");}
            else if(userInput.isEmpty()){printError("No input!");}
            else if(userInput.equalsIgnoreCase("true") ||
		    userInput.equalsIgnoreCase("t") ||
                    userInput.equalsIgnoreCase("y") ||
                    userInput.equalsIgnoreCase("yes") ||
                    userInput.equalsIgnoreCase("yep") ||
                    userInput.equalsIgnoreCase("yeah")||
                    userInput.equals("1"))
            {return true;}
            else if(userInput.equalsIgnoreCase("false") ||
		    userInput.equalsIgnoreCase("f") ||
                    userInput.equalsIgnoreCase("n") ||
                    userInput.equalsIgnoreCase("no") ||
                    userInput.equalsIgnoreCase("nope") ||
                    userInput.equalsIgnoreCase("nah") ||
                    userInput.equals("0"))
            {return false;}
            else{printError("Unrecognised response!");}
            println("Please try again!");
        }
    }

    private String readString()
    {
        String userInput = term.nextLine();
        return userInput;
    }

    private void printError(String msg){println("Error. " + msg);}

    private void print(String msg){System.out.print(msg);}

    private void println(String msg){System.out.println(msg);
    }
}
