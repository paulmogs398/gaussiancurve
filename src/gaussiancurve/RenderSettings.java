package gaussiancurve;

public class RenderSettings
{
    private double startAt;
    private double interval;
    private double endBefore;
    private boolean drawCrosses;
    private boolean printValues;
    private boolean drawKey;
    private int imageXSize;
    private int imageYSize;
    private String fileName;

    public RenderSettings(){}

    public void setStartAt(double startAt){this.startAt = startAt;}
    public double getStartAt(){return this.startAt;}
    public void setInterval(double interval){this.interval = interval;}
    public double getInterval(){return this.interval;}
    public void setEndBefore(double endBefore){this.endBefore=endBefore;}
    public double getEndBefore(){return this.endBefore;}
    public void setDrawCrosses(boolean drawCrosses){this.drawCrosses=drawCrosses;}
    public boolean getDrawCrosses(){return this.drawCrosses;}
    public void setPrintValues(boolean printValues){this.printValues=printValues;}
    public boolean getPrintValues(){return this.printValues;}
    public void setDrawKey(boolean drawKey){this.drawKey = drawKey;}
    public boolean getDrawKey(){return this.drawKey;}
    public void setImageXSize(int imageXSize){this.imageXSize=imageXSize;}
    public int getImageXSize(){return this.imageXSize;}
    public void setImageYSize(int imageYSize){this.imageYSize=imageYSize;}
    public int getImageYSize(){return this.imageYSize;}
    public void setFileName(String fileName){this.fileName = fileName;}
    public String getFileName(){return this.fileName;}
}
