package gaussiancurve;

public class DataPoint 
{
    private final double X;
    private final double Y;
    private final String SERIES;
    
    public DataPoint(double x, double y, String series)
    {
        this.X = x;
        this.Y = y;
        this.SERIES = series;
    }
    
    public double getX(){return X;}
    public double getY(){return Y;}
    public String getSeries(){return SERIES;}
}
