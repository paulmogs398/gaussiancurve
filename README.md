# README #

GaussianCurve is a little Java application for drawing Gaussian curves from given values.

### Example Usage ###
	
	### Gaussian Curve Interactive Mode ###
	Please enter a Sigma: (This influences the width of Gaussian Curve)
	0.3
	Please enter a Mu: (This positions the Gaussian Curve)
	50
	Please enter a name for this series/curve.
	Series One 
	Would you like to add another series?
	y
	Please enter a Sigma: (This influences the width of Gaussian Curve)
	1.2
	Please enter a Mu: (This positions the Gaussian Curve)
	50
	Please enter a name for this series/curve.
	Series Two 
	Would you like to add another series?
	y
	Please enter a Sigma: (This influences the width of Gaussian Curve)
	5
	Please enter a Mu: (This positions the Gaussian Curve)
	35
	Please enter a name for this series/curve.
	Series Three
	Would you like to add another series?
	n
	Render the curves from: 
	0
	Sampling every: 
	0.2
	Until: 
	80
	Draw "x"'s to mark samples?
	n
	Print out the values here?
	n
	Display a key on image?
	y
	Please enter the size of the image you wish to render.
	 x: 800
	 y: 600
	Please enter a name for the file: (w/o extension)
	TestImage
	Plotting curve(s)...
	Printing values...
	Rendering...
	Done!
	#@#:~/Documents/GaussianCurve/gaussiancurve$ eog TestImage.gif 

If you have any problems, questions comments or advice to improve this application please email paulmogs398@gmail.com  many thanks :D
